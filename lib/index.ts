import { TypeOf, NumericRange, Thunk, Class, Parity } from "@valuer/types";

import { fn } from "@valuer/fn";

/** @private */
const getParity = (integer: number): Parity =>
	!Number.isInteger(integer)? undefined : (integer % 2)? "odd" : "even";

/** @private */
const toRange = (numbers: number[]): NumericRange =>
	[ Math.min(...numbers), Math.max(...numbers) ];

// ***

export namespace is {
	/**
	 * Get is-of-type thunk
	 * @arg types One or more JS-type(s)
	 */
	export const ofType = (...types: TypeOf[]): Thunk<any> => value =>
		types.some(type => typeof value === type);

	/**
	 * Get is-an-instance-of thunk
	 * @arg classes One or more class
	 */
	export const instanceOf = (...classes: Class[]): Thunk<any> => value =>
		classes.some(klass => value instanceof klass);

	/**
	 * Get is-a-direct-instance-of thunk
	 * @arg classes One or more class
	 */
	export const directInstanceOf = (...classes: Class[]): Thunk<any> => value =>
		value != null && classes.some(klass => value.constructor === klass);

	// ***

	/** Whether the value is of type "string" */
	export const _string = (value: any): value is string =>
		ofType("string")(value);

	/** Whether the value is of type "number" */
	export const _number = (value: any): value is number =>
		ofType("number")(value);

	/** Whether the value is of type "boolean" */
	export const _boolean = (value: any): value is boolean =>
		ofType("boolean")(value);

	/** Whether the value is of type "symbol" */
	export const _symbol = (value: any): value is symbol =>
		ofType("symbol")(value);

	/** Whether the value is of type "function" */
	export const _function = (value: any): value is Function =>
		ofType("function")(value);

	/** Whether the value is of type "object" */
	export const _object = (value: any): value is object | null =>
		ofType("object")(value);

	/** Whether the value is `undefined` */
	export const _undefined = (value: any): value is undefined =>
		value === undefined;

	/** Whether the value is `null` */
	export const _null = (value: any): value is null =>
		value === null;

	// ***

	/** Whether the value is a void */
	export const _void = (value: any): value is void =>
		value == null;

	/** Whether the value is not a void */
	export const _nonVoid: Thunk = fn.not(_void);

	/** Whether the value is of type "object", but not `null` */
	export const _objectNonNull = (value: any): value is object =>
		fn.all(_object, _nonVoid)(value);

	/** Whether the value is a primitive */
	export const _primitive = (value: any): value is string | number | boolean | symbol | undefined | null =>
		fn.any(ofType("string", "number", "boolean", "symbol", "undefined"), _null)(value);

	/** Whether the value is a primitive and is not a void */
	export const _nonVoidPrimitive = (value: any): value is string | number | boolean | symbol =>
		fn.all(_primitive, _nonVoid)(value);

	/** Whether the value is composite */
	export const _composite = (value: any): value is object | Function | null =>
		ofType("object", "function")(value);

	/** Whether the value is composite and is not a void */
	export const _nonVoidComposite = (value: any): value is object | Function =>
		fn.all(_composite, _nonVoid)(value);

	// ***

	/**
	 * Get is-an-array thunk
	 * @arg length Length of a valid array
	 * @arg valid Validity check for each element of the array
	 */
	export const array = <T>(length?: number, valid?: Thunk<T>): Thunk<T[]> => array => {
		if (!Array.isArray(array))
			return false;

		else if (length != null && array.length !== length)
			return false;

		else if (valid != null && array.some(fn.not(valid)))
			return false;

		else return true;
	};

	// ***

	/** Get is-not-NaN thunk */
	export const numeric = (): Thunk<number> => value =>
		!Number.isNaN(value);

	/**
	 * Get is-in-range thunk
	 * @arg range The range
	 * @arg inclusively (defaults to `true`) Whether to count boundary match
	 */
	export const inRange = (range: NumericRange, inclusively: boolean = true): Thunk<number> => value => {
		const [ min, max ] = toRange(range);

		return inclusively ? (value >= min && value <= max) : (value > min && value < max);
	};

	/** Get is-not-an-integer thunk */
	export const float = (): Thunk<number> => value =>
		!Number.isInteger(value);

	/**
	 * Get is-an-integer thunk
	 * @arg parity (optional) Required parity of an integer
	 */
	export const integer = (parity?: Parity): Thunk<number> => value => {
		if (float()(value))
			return false;

		else if (parity != null)
			return getParity(value) === parity;

		else return true;
	};

	/**
	 * Get is-a-natural-number thunk
	 * @arg zero (defaults to `true`) Whether zero is considered a natural number
	 */
	export const natural = (zero: boolean = true): Thunk<number> => value =>
		integer()(value) && value >= (+!zero);

	// ***

	/**
	 * Get is-a-boolean thunk
	 * @arg expected (optional) Expected boolean identity of the value
	 */
	export const boolean = (expected?: boolean): Thunk<boolean> => value =>
		_boolean(value) && (_void(expected) || value === expected);

	// ***

	/** Unconstrainted thunk */
	export const _any = <Anything>(value: any): value is Anything =>
		true;
}
